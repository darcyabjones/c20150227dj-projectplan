# Project plan
The project plan will help you structure your project, think about the methods used in depth and solidly plan the year ahead.

- Maximum 2500 words.

Should include:

- Aims and significance of the work
- Specific statements of the hypotheses to be addressed
- Detailed methods for laboratory and field work and statistical analysis
- Time plan for assessment tasks, field and laboratory work, analysis and write-up
- Draft thesis structure
- Expected outcomes
